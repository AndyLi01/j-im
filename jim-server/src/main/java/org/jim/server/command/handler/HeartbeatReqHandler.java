package org.jim.server.command.handler;

import org.jim.core.ImChannelContext;
import org.jim.core.ImPacket;
import org.jim.core.exception.ImException;
import org.jim.core.packets.Command;
import org.jim.core.packets.HeartbeatBody;
import org.jim.core.packets.ImClientNode;
import org.jim.core.packets.RespBody;
import org.jim.server.command.AbstractCmdHandler;
import org.jim.server.protocol.ProtocolManager;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 */
public class HeartbeatReqHandler extends AbstractCmdHandler{
	Logger log = LoggerFactory.getLogger(HeartbeatReqHandler.class);
	@Override
	public ImPacket handler(ImPacket packet, ImChannelContext channelContext) throws ImException {
		ImClientNode imClientNode = channelContext.getSessionContext().getImClientNode();
		log.info("收到{},{}的心跳指令，当前连接存活",imClientNode.getIp(),imClientNode.getPort());
		RespBody heartbeatBody = new RespBody(Command.COMMAND_HEARTBEAT_REQ).setData(new HeartbeatBody(Protocol.HEARTBEAT_BYTE));
		ImPacket heartbeatPacket = ProtocolManager.Converter.respPacket(heartbeatBody,channelContext);
		return heartbeatPacket;
	}

	@Override
	public Command command() {
		return Command.COMMAND_HEARTBEAT_REQ;
	}
}
